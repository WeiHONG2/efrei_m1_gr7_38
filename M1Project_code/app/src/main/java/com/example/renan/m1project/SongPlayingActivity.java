package com.example.renan.m1project;

import android.content.ContentResolver;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

public class SongPlayingActivity extends AppCompatActivity {
    private String IPAddress = "127.0.0.1";
    private MediaPlayer mediaPlayer;
    private boolean isStopped = false;
    private int songID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_song_playing);

        songID = (int)getIntent().getSerializableExtra("songID");

            // REMOTE
           // String url = "http://" + IPAddress + "/songs/" + threeDigitFormat(songID)+".mp3";
          /*  String url = "http://192.168.1.37/songs/001.mp3";
            MediaPlayer mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(url);
            mediaPlayer.prepare();
            mediaPlayer.start();*/
        mediaPlayer = new MediaPlayer();
        initMediaPlayer();
    }

    private void initMediaPlayer() {
        try {
            mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.song_001);
           /* ContentResolver musicResolver = getContentResolver();
            Uri musicUri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

            Log.e("mediaactivity",musicUri.getPath());
            Log.e("mediaactivity",a);
            if(file.exists())
            {
                Log.e("mediaactivity","true");
            }
            if(!file.exists())
            {
                Log.e("mediaactivity","false");
            }
            mediaPlayer.setDataSource("/external/audio/media/wave.mp3");*/
            //mediaPlayer.setDataSource(this,R.raw.music);
            // mediaPlayer.setDataSource(file.getPath());
           // mediaPlayer.prepare();
            mediaPlayer.start();
      /*  String path ="/sdcard/music.mp3";
        mediaPlayer.setDataSource(path);
        mediaPlayer.prepare();
        mediaPlayer.start();*/
        }
        catch (Exception e)
        {
            e.printStackTrace();

        }
    }

    public void pause(View view) {
        mediaPlayer.pause();
        Log.d("Log","pause");
    }

    public void stop(View view) {
      mediaPlayer.stop();
      isStopped = true;
        Log.d("Log","stop");
    }

    public void play(View view) {
        if(isStopped) {
           initMediaPlayer();
            isStopped = false;
        }
       mediaPlayer.start();
       Log.d("Log","play");
    }

    public void nextSong(View view) {
        mediaPlayer.stop();
        Log.d("Log","nextSong");
        /*songID
        Intent intent = new Intent(this, SongPlayingActivity.class);
        intent.putExtra("songID", songID);
        startActivity(intent);*/
    }
}